all : tp1 test

tp1: main.o lco.o util.o agenda.o
	gcc -o tp1 main.o lco.o util.o agenda.o -W -Wall

test: test.o lco.o util.o agenda.o
	gcc -o test test.o lco.o util.o agenda.o -W -Wall

main.o : main.c lco.h
	gcc -c main.c -W -Wall

lco.o : lco.c lco.h
	gcc -c lco.c -W -Wall

util.o : util.c util.h
	gcc -c util.c -W -Wall

agenda.o : agenda.c agenda.h
	gcc -c agenda.c -W -Wall

test.o : test.c test.h
	gcc -c test.c -W -Wall

clean :
	rm -f tp1 *.o test