/*------------------------------------------------------------------------------------------------------*/
/*        lco.c          fonctions relatives à la liste contiguë avec pointeur de fin de type lco_t     */
/* -----------------------------------------------------------------------------------------------------*/

#include "lco.h"

/*----------------------------------------------------------------------------------------------*/
/* listeJours      crée la liste contiguë des jours où se passe une action contenant un motif   */
/*                                                                                              */
/* En entrée :  agenda       tête de l'agenda considéré                                         */
/*              motif        motif contenu dans le nom des actions considérées                  */
/*                                                                                              */
/* En sortie :  lco          pointeur sur la liste contiguë des jours (de type lco_t*)          */
/*                                                                                              */
/* Localement : courAge      pointe sur la semaine courante                                     */
/*              courAct      pointe sur l'action courante                                       */
/*              jour         numéro du jour dernièrement ajouté                                 */
/*              i            indice de liste lors des parcours                                  */
/*----------------------------------------------------------------------------------------------*/

lco_t* listeJours(agenda_t* agenda, char* motif)
{
    lco_t     * lco=NULL;
    if(agenda!=NULL)
    {
        agenda_t  * courAge = agenda;
        action_t  * courAct;
        int         jour,
                    i = 0;
        lco = (lco_t*) malloc (sizeof(lco_t));

        if (lco != NULL)
        {

            lco->fin = NULL;

            for (i=0; i<TAILLEMAX; i++)
            {
                (lco->liste)[i] = 0;
            }

            i=0;

            //Tant qu'on n'a pas parcouru toute la liste des semaines
            while (courAge != NULL)
            {
                
                courAct = courAge->action;
                jour = 0;               //variable jour pour éviter les doublons

                //tant qu'on n'a pas parcouru toute la liste des actions
                while (courAct != NULL)
                {

                    //Si le jour n'est pas déjà dans la liste et que l'action contient le motif
                    if (courAct->jourHeure[0] != jour && contient(courAct->nomAction, motif))
                    {

                        //On copie le jour dans la liste
                        jour = courAct->jourHeure[0];
                        memcpy(&(lco->liste)[i], courAge->annee, 4);
                        i+=4;
                        memcpy(&(lco->liste)[i], courAge->numSem, 2);
                        i+=2;
                        (lco->liste)[i] = jour;
                        i++;

                        if (lco->fin == NULL)
                        {
                            lco->fin = lco->liste + 6;
                        }
                        else
                        {
                            lco->fin += 7;
                        }
                    }
                    courAct = courAct->suiv;
                }
                courAge = courAge->suiv;
            }
        }
    }
    return lco;
}

/*----------------------------------------------------------------*/
/* afficherLco    affiche la liste des jours contenant un motif   */
/*                                                                */
/* En entrée :  lco     liste contiguë des jours                  */
/*                                                                */
/* En sortie :  aucune                                            */
/*                                                                */
/* Localement : cour    pointe sur le jour courant                */
/*----------------------------------------------------------------*/

void afficherLco(lco_t * lco)
{
    char    * cour = lco->liste;

    if (lco->fin != NULL)
    {
        //tant que le jour courant n'est pas le dernier
        while(cour+6 < lco->fin)
        {
            afficherTab(cour, stdout, 7);
            printf("\n");
            cour = cour+7;
        }
        //on affiche une dernière fois
        afficherTab(cour, stdout, 7);
            printf("\n");
    }
}