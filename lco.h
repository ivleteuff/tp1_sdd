/*-------------------------------------------------------------------------------------------------------------------*/
/*       lco.h        déclaration de la structure lco_t et de ses fonctions et constantes symboliques relatives      */
/* ------------------------------------------------------------------------------------------------------------------*/

#ifndef LCO_H
#define LCO_H

#include "agenda.h"

#define TAILLEMAX 49 //7 jours traités

typedef struct lco
{
    char    liste[TAILLEMAX];
    char  * fin;
} lco_t;


lco_t* listeJours(agenda_t* agenda, char* motif);
void afficherLco(lco_t *lco);

#endif