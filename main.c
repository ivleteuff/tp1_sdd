/*------------------------------------------------------------------------------------------------------------*/
/*             main.c            aperçu des différentes fonctionnalités de l'agenda et de la lco              */
/* -----------------------------------------------------------------------------------------------------------*/

#include "lco.h"

int main(int argc, char** argv)
{
    agenda_t    * agenda;
    lco_t       * lco;

    if (argc != 2)
    {
        printf("Veuillez donner le nom du fichier de données en paramètre\n");
    }
    else
    {
        agenda = initialiseAgenda(argv[1]);

        afficherSDD(agenda);

        lco = listeJours(agenda, "nains");

        printf("lco des jours de 'nains'\n");
        afficherLco(lco);

        supprimerActions(&agenda, "202114", "100");

        sauvegarderSDD(agenda, "sauv.txt");

        free(lco);

        libererSDD(&agenda);
    }
    return 0;
}