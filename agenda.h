/*------------------------------------------------------------------------------------------------------------*/
/*       agenda.h            déclaration des structures action_t et agenda_t et leurs fonctions relatives     */
/* -----------------------------------------------------------------------------------------------------------*/

#ifndef AGENDA_H
#define AGENDA_H


#include <stdlib.h>
#include <string.h>

#include "util.h"

typedef struct action
{
    char            jourHeure[3];
    char            nomAction[10];
    struct action * suiv;
} action_t;

typedef struct agenda 
{
    char            annee[4];
    char            numSem[2];
    action_t      * action;
    struct agenda * suiv;
} agenda_t;

agenda_t* initListe();
action_t** testSiAjout(agenda_t** tagenda, char anneeSemaine[]);
void ajoutSemaine(agenda_t* semaine, agenda_t** prec);
void ajoutTache(action_t* tache, action_t** tactions);
agenda_t* initialiseAgenda(char* nomFic);
void sauvegarderSDD(agenda_t* agenda, char* nomFic);
void afficherSDD(agenda_t* agenda);
void afficherTache(action_t* tache);
void supprimerActions(agenda_t** tagenda, char anneeSemaine[], char jourHeure[]);
void libererSDD(agenda_t** tagenda);

#endif