#include "test.h"
#include "agenda.h"

void testInitialiserAgenda(){
	agenda_t* agenda;

	printf("TEST : InitialiserAgenda\n\n");

	//Test fichier inexistant
	agenda = initialiseAgenda("fichierInexistant");
	if(agenda==NULL){
		printf("test fichier inexistant : Réussi\n");
	}
	else{
		printf("test fichier inexistant : Échoué\n");
	}

	//test fichier vide
	agenda = initialiseAgenda("fichiersTest/ficVide.txt");
	if(agenda==NULL){
		printf("test fichier vide : Réussi\n");
	}
	else{
		printf("test fichier vide : Échoué\n");
	}

	//test fichier mauvais format
	agenda = initialiseAgenda("fichiersTest/ficMauvaisFormat.txt");
	if(agenda==NULL){
		printf("test fichier mauvais format : Réussi\n");
	}
	else{
		printf("test fichier mauvais format : Échoué\n");
	}

	printf("\ntest fichier au bon format(donnee.txt) :\n");
	agenda = initialiseAgenda("donnee.txt");
	afficherSDD(agenda);
}

void testSauvegarderSDD(){
	agenda_t* agenda;
	int egaux=0;

	printf("TEST : sauvegarderSDD\n\n");
	//Cas fichier agenda NULL
	agenda=NULL;
	sauvegarderSDD(agenda,"fichiersTest/testSauvSDDNULL.txt");
	FILE* fic= fopen("fichiersTest/testSauvSDDNULL.txt","r");//ouverture du fichier de test
	int c=0;
	c=fgetc(fic);

	//On test si c'est la fin du fichier
	if(c==EOF)
		printf("test sauvegarderSDD agenda NULL : Réussi\n");
	else
		printf("test sauvegarderSDD NULL: Échoué\n");
	fclose(fic);

	//Test conditions normales
	agenda = initialiseAgenda("donnee.txt");
	sauvegarderSDD(agenda,"fichiersTest/testSauvSDD.txt");
	//Ouverture du fichier trié correspondant au résultat attendu
	FILE* fic1= fopen("fichiersTest/testSauvSDDResultatAttendu.txt","r");
	FILE* fic2= fopen("fichiersTest/testSauvSDD.txt","r");//Création du fichier de test
	char x[30];
	char y[30];

	//On regarde si chaque ligne est identique
	while(egaux==0 && !feof(fic1) && !feof(fic2)){

		fgets(x,20,fic1);
		fgets(y,20,fic2);
		 
		if(strcmp(x,y) !=0)
			egaux++;
	}

	if(egaux==0 && feof(fic1) && feof(fic2))
		printf("test sauvegarderSDD : Réussi\n");
	else
		printf("test sauvegarderSDD : Échoué\n");

	fclose(fic1);
	fclose(fic2);
}


void testSupprimerActions(){
	agenda_t* agenda;
	printf("TEST : supprimerActions\n\n");

	//cas agenda NULL (On regarde si ça fait une segfault ou non)
	agenda=NULL;
	supprimerActions(&agenda, "202108", "106");
	printf("test supprimerActions avec agenda NULL : Réussi\n");
	
	//cas action inexistante
	agenda = initialiseAgenda("donnee.txt");
	supprimerActions(&agenda, "202108", "106");//Action non dans l'agenda
	
	printf("\ntest supprimerActions non présente dans l'agenda :\n");
	afficherSDD(agenda);


	//cas action existante sans supprimer la semaine
	agenda = initialiseAgenda("donnee.txt");
	supprimerActions(&agenda, "202114", "410");//Action présente dans l'agenda, action Infimerie

	printf("\ntest supprimerActions présente dans l'agenda :\n");
	afficherSDD(agenda);


	//cas action existante en supprimant la semaine
	agenda = initialiseAgenda("donnee.txt");
	supprimerActions(&agenda, "202113", "410");//Action présente dans l'agenda, action Cuisine
  
	printf("\ntest supprimerActions présente dans l'agenda avec suppression semaine:\n");
	afficherSDD(agenda);

}

void testlco(){
	agenda_t* agenda;
	lco_t* lco;

	printf("TEST : lco\n\n");

	//Cas agenda NULL
	agenda = NULL;
	lco = listeJours(agenda, "nains");
	if(lco==NULL)
		printf("test lco agenda NULL : Réussi\n");
	else
		printf("test lco agenda NULL : Échoué\n");

	//Cas motif présent dans aucune action
	printf("\ntest lco motif présent dans aucune action :\n");
	agenda = initialiseAgenda("donnee.txt");
	lco = listeJours(agenda, "gloubi");
	afficherLco(lco);
	
	//Cas motif présent dans une ou plusieurs actions
	printf("\ntest lco motif présent dans une ou plusieurs actions :\n");
	agenda = initialiseAgenda("donnee.txt");
	lco = listeJours(agenda, "nain");
	afficherLco(lco);
}


int main(){
	//testInitialiserAgenda();
	//testSauvegarderSDD();
	//testSupprimerActions();
	testlco();
}