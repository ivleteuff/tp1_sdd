/*------------------------------------------------------------------------------------------------------*/
/*        util.h          déckaration des fonctions utilitaires utilisées un peu partout dans le TP     */
/* -----------------------------------------------------------------------------------------------------*/

#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

FILE* ouvertureFichier(char* nomFic, char* mode);
void afficherTab(char* charTab, FILE* fichier, int taille);
int compareTab(char* charTab1, char* charTab2, int taille);
int contient(char* charTab, char* motif);

#endif