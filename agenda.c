/*------------------------------------------------------------------------------------------------------*/
/*              agenda.c              fonctions relatives à la structure agenda_t                       */
/* -----------------------------------------------------------------------------------------------------*/

#include "agenda.h"

/*---------------------------------------------------*/
/* initListe       initialise un agenda à NULL       */
/*                                                   */
/* En entrée : aucune                                */
/*                                                   */
/* En sortie : la tête d'un agenda_t vide            */
/*---------------------------------------------------*/

agenda_t* initListe()
{
    return NULL;
}

/*--------------------------------------------------------------------------------------------------------------*/
/* testSiAjout            Ajoute le bloc d'année et de semaine donnée si nécessaire                             */
/*                                                                                                              */
/* En entrée :  tagenda         pointeur sur la tête de la liste de type agenda_t considérée (type agenda_t**)  */
/*              anneeSemaine    le tableau de six caractères contenant l'année et la semaine du bloc à insérer  */
/*                                                                                                              */
/* En sortie :  pointeur sur la tête de la liste des actions de la semaine considérée (type action_t**)         */
/*                                                                                                              */
/* Localement : prec            pointeur sur le suiv du bloc précédent de la semaine courante                   */
/*                              maillon   nouvelle semaine à insérer                                            */
/*--------------------------------------------------------------------------------------------------------------*/

action_t** testSiAjout(agenda_t** tagenda, char anneeSemaine[])
{
    agenda_t  ** prec = tagenda;  //pointe sur le suiv du bloc précédent de la semaine courante

    //tant que le parcours de la liste n'est pas terminé et que la semaine courante est strictement inférieure à la semaine à insérer
    while (*prec != NULL && compareTab(anneeSemaine, (*prec)->annee, 6) > 0)
    {
        prec = &((*prec)->suiv);
    }

    //si le parcours de la liste est terminé ou que la semaine courante est supérieure à celle à insérer
    if (*prec == NULL || compareTab(anneeSemaine, (*prec)->annee, 6) != 0)
    {

        //insertion de la nouvelle semaine
        agenda_t  * maillon = (agenda_t*)malloc(sizeof(agenda_t));

        if (maillon != NULL)
        {
            memcpy(maillon->annee, anneeSemaine, 6);
            maillon->action = NULL;
            maillon->suiv = NULL;

            ajoutSemaine(maillon, prec);
        }
    }
    return &((*prec)->action);
}

/*--------------------------------------------------------------------------------------------------*/
/* ajoutSemaine         ajoute la semaine                                                           */
/*                                                                                                  */
/* En entrée : semaine      pointeur sur le bloc à ajouter                                          */
/*             prec         pointeur sur le suiv du bloc après lequel la semaine doit être ajoutée  */
/*                                                                                                  */
/* En sortie : aucune                                                                               */
/*--------------------------------------------------------------------------------------------------*/

void ajoutSemaine(agenda_t* semaine, agenda_t** prec)
{
    semaine->suiv = *prec;
    *prec = semaine;
}

/*--------------------------------------------------------------------------------------------*/
/* ajoutTache         ajoute la tâche au bon endroit                                          */
/*                                                                                            */
/* En entrée :  tache      pointeur sur le bloc à ajouter                                     */
/*              tactions   pointeur sur la tête de la liste des actions de type action_t      */
/*                                                                                            */
/* En sortie :  aucune                                                                        */
/*                                                                                            */
/* Localement : prec       pointeur sur le suiv du bloc précédent de l'action courante        */
/*--------------------------------------------------------------------------------------------*/

void ajoutTache(action_t* tache, action_t** tactions)
{
    action_t  ** prec = tactions; //pointe sur le suiv du bloc précédent de l'action courante

    //Si la tête de la liste n'est pas NULL
    if (*prec != NULL)
    {

        //tant que le parcours de la liste n'est pas terminé et que l'heure courante est chronologiquement avant l'heure de la tâche à insérer (jour et heure)
        while ((*prec)->suiv != NULL && (*prec)->jourHeure < tache->jourHeure)
        {
            
            prec = &(*prec)->suiv;
        }

        //insersion de la tâche
        tache->suiv = (*prec)->suiv;
        (*prec)->suiv = tache;
    }

    //Si la tête de la liste est NULL
    else
    {
        *prec = tache;
    }
}

/*--------------------------------------------------------------------------------------------------------------------------*/
/* initialiseAgenda      crée l'agenda à partir du fichier texte                                                            */
/*                                                                                                                          */
/* En entrée :  nomFic        chaîne de caractère contenant le nom du fichier                                               */
/*                                                                                                                          */
/* En sortie :  agenda        tête de l'agenda ainsi créé                                                                   */
/*                                                                                                                          */
/* Localement : chaine        chaine de caractère contenant la ligne du fichier en cours d'insertion                        */
/*              anneeSemaine  tableau de six caractères contenant l'annee et la semaine de la ligne en cours d'insertion    */
/*              jourHeure     tableau de trois caractères contenant le jour et l'heure de la ligne en cours d'insertion     */
/*              nomAction     tableau de dix caractères contenant le nom de l'action de la ligne en cours d'insertion       */
/*              tactions      pointeur sur la tête de la liste des actions en cours de traitement                           */
/*              tache         pointeur sur la tache de type action_t en cours d'insertion                                   */
/*              fichier       fichier ouvert de nom nomFic                                                                  */
/*--------------------------------------------------------------------------------------------------------------------------*/

agenda_t* initialiseAgenda(char nomFic[])
{
    char         chaine[20],
                 anneeSemaine[6],
                 jourHeure[3],
                 nomAction[10];
    agenda_t   * agenda = initListe();
    action_t  ** tactions,
               * tache;
    FILE       * fichier = ouvertureFichier(nomFic, "r");

    if (fichier != NULL)
    {

        //Tant qu'on n'a pas atteint la fin du fichier
        while(!feof(fichier))
        {

            //On lit une ligne du fichier et on la répartit dans les variables adéquates
            fgets(chaine, 21, fichier);

            //Si le nombre de caractères correspond au format attendu
            if (strlen(chaine)==20 || strlen(chaine)==19)
            {
                memcpy(anneeSemaine, chaine, 6);
                memcpy(jourHeure, chaine+6, 3);
                memcpy(nomAction, chaine+9, 10);            

                tactions = testSiAjout(&agenda, anneeSemaine);
                
                tache = (action_t*)malloc(sizeof(action_t));

                if (tache != NULL)
                {
                    //On remplit le bloc pointé par tache et on l'ajoute dans la structure
                    memcpy(tache->jourHeure, jourHeure, 3);
                    memcpy(tache->nomAction, nomAction, 10);
                    tache->suiv = NULL;

                    ajoutTache(tache, tactions);
                    
                }
            }
        }
        fclose(fichier);

    }
    return agenda;
}

/*--------------------------------------------------------------------------------------------------------*/
/* sauvegarderSDD      écrit la structure de données dans un fichier texte                                */
/*                                                                                                        */
/* En entrée :  agenda      tête de l'agenda à sauvegarder                                                */
/*              nomFic      chaîne de caractères contenant le nom du fichier dans lequel sauvegarder      */
/*                                                                                                        */
/* En sortie :  aucune                                                                                    */
/*                                                                                                        */
/* Localement : fichier     fichier ouvert de nom nomFic                                                  */
/*              courAge     pointe sur la semaine courante                                                */
/*              courAct     pointe sur l'action courante                                                  */
/*              chaine      tableau de dix-neuf caractères contenant la ligne à inscrire dans le fichier  */
/*--------------------------------------------------------------------------------------------------------*/

void sauvegarderSDD(agenda_t* agenda, char* nomFic)
{
    FILE  * fichier;

    fichier = ouvertureFichier(nomFic, "w");

    if(fichier != NULL)
    {
        agenda_t  * courAge = agenda;
        action_t  * courAct;
        char        chaine[19];

        //tant que le parcours de la liste des semaines n'est pas terminée
        while(courAge != NULL)
        {   
            //On remplit le début de la chaîne avec l'année et la semaine
            memcpy(chaine, courAge->annee, 4);
            memcpy(chaine+4, courAge->numSem, 2);

            courAct = courAge->action;

            //tant que le parcours de la liste des actions n'est pas terminé
            while (courAct != NULL)
            {   
                //On remplit la fin de la chaîne avec les autres informations
                memcpy(chaine+6, courAct->jourHeure, 3);
                memcpy(chaine+9, courAct->nomAction, 10);

                afficherTab(chaine, fichier, 19);

                //Si la ligne remplie n'est pas la dernière
                if (courAge->suiv != NULL || courAct->suiv != NULL)
                {
                    fprintf(fichier, "\n");
                }

                courAct = courAct->suiv;
            }
            courAge = courAge->suiv;
        }
        fclose(fichier);
    }
}

/*---------------------------------------------------------------------------------------------------*/
/* supprimerAction       supprime toutes les actions d'année, de semine, de jour et d'heure données  */
/*                                                                                                   */
/* En entrée :  tagenda       pointeur sur la tête de l'agenda                                       */
/*              anneeSemaine  tableau de 6 caractères contenant l'année et la semaine                */
/*              jourHeure     tableau de 3 caractères contenant le jour et l'heure                   */
/*                                                                                                   */
/* En sortie :  aucune                                                                               */
/*                                                                                                   */
/* Localement : precAge       pointe sur le suiv du bloc précédent de la semaine courante            */
/*              precAct       pointe sur le suiv du bloc précédent de l'action courante              */
/*              stockAge      suiv de l'action en cours de suppression                               */
/*              stockAct      suiv de la semaine en cours de suppression                             */
/*---------------------------------------------------------------------------------------------------*/

void supprimerActions(agenda_t** tagenda, char anneeSemaine[], char jourHeure[])
{
    if(*tagenda !=NULL){
        agenda_t    ** precAge = tagenda,  //pointe sur le suiv du bloc précédent la semaine courante
                     * stockAge;
        action_t    ** precAct,
                     * stockAct;

        //tant que le parcours de la liste n'est pas terminé et que la semaine est supérieure à la semaine courante
        while (*precAge != NULL && compareTab(anneeSemaine, (*precAge)->annee, 6) > 0)
        {
            //On avance
            precAge = &( (*precAge)->suiv );
        }

        //Si la semaine obtenue est celle voulue
        if (*precAge != NULL && compareTab(anneeSemaine, (*precAge)->annee, 6) == 0)
        {
            precAct = &( (*precAge)->action );

            //tant que le parcours de la liste n'est pas terminé et que l'heure est supérieure à l'heure courante
            while (*precAct != NULL && compareTab(jourHeure, (*precAct)->jourHeure, 3) > 0)
            {
                precAct = &( (*precAct)->suiv );
            }

            //Pour tous les blocs de l'heure voulue
            while (*precAct != NULL && compareTab(jourHeure, (*precAct)->jourHeure, 3) == 0)
            {
                //On supprime le bloc
                stockAct = (*precAct)->suiv;

                free( *precAct );

                *precAct = stockAct;
            }
        }

        //S'il n'y a plus d'actions dans la semaine
        if ( (*precAge)->action == NULL )
        {
            //On supprime la semaine
            stockAge = (*precAge)->suiv;

            free( (*precAge) );

            *precAge = stockAge;
        }
    }
}

/*--------------------------------------------------------------------------------------*/
/* libererSDD      libère la structure de type agenda_t                                 */
/*                                                                                      */
/* En entrée :  tagenda   pointeur sur la tête de l'agenda à libérer                    */
/*                                                                                      */
/* En sortie :  aucune                                                                  */
/*                                                                                      */
/* Localement : courAge   pointe sur la semaine courante                                */
/*              stockAge  pointe sur la semaine en cours de suppression                 */
/*              courAct   pointe sur l'action courante                                  */
/*              stockAct  pointe sur l'action en cours de suppression                   */
/*--------------------------------------------------------------------------------------*/

void libererSDD(agenda_t** tagenda)
{
    agenda_t    * courAge  = *tagenda,
                * stockAge =  courAge;
    action_t    * courAct,
                * stockAct;

    //parcours de la liste des semaines
    while (courAge != NULL)
    {
        courAct = stockAge->action;
        stockAct = courAct;

        //parcours de la liste des actions
        while (courAct != NULL)
        {
            //libération de l'action et passage au suivant
            courAct =  courAct->suiv ;
            free(stockAct);
            stockAct = courAct;
        }

        //libération de la semaine et passage au suivant
        courAge =  courAge->suiv ;
        free(stockAge);
        stockAge = courAge;
    }
}

/*-------------------------------------------------------*/
/* afficherSDD     affiche la structure                  */
/*                                                       */
/* En entrée :  agenda   tête de la structure            */
/*                                                       */
/* En sortie :  aucune                                   */
/*                                                       */
/* Localement : cour     pointe sur la semaine courante  */
/*-------------------------------------------------------*/

void afficherSDD(agenda_t* agenda)
{
    agenda_t  * cour = agenda;

    //Parcours de la liste des semaines
    while (cour != NULL)
    {
        printf("année : ");
        afficherTab(cour->annee, stdout, 4);
        printf(", semaine : ");
        afficherTab(cour->numSem, stdout, 2);
        printf("\n");

        afficherTache(cour->action);

        printf("\n");

        cour = cour->suiv;
    }
}

/*---------------------------------------------------*/
/* afficherTache    affiche la liste des actions     */
/*                                                   */
/* En entrée :  tache   tête de la liste des actions */
/*                                                   */
/* En sortie :  aucune                               */
/*                                                   */
/* Localement : cour    pointe sur l'action courante */
/*---------------------------------------------------*/

void afficherTache(action_t* tache)
{
    action_t  * cour = tache;

    //Parcours de la liste des actions
    while (cour != NULL)
    {
        printf("jour : %c, ", cour->jourHeure[0]);
        printf("heure : ");
        afficherTab(cour->jourHeure+1, stdout, 2);
        printf(", action : ");
        afficherTab(cour->nomAction, stdout, 10);
        printf("\n");

        cour = cour->suiv;
    }

}