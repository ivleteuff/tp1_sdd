
#ifndef TEST_H
#define TEST_H


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lco.h"

void testInitialiserAgenda();
void testSauvegarderSDD();
void testSupprimerActions();
void testlco();

#endif