/*------------------------------------------------------------------------------------------------------*/
/*           util.c              fonctions utilitaires utilisées un peu partout dans le TP              */
/* -----------------------------------------------------------------------------------------------------*/

#include "util.h"

/*----------------------------------------------------------------------------*/
/* ouvertureFichier      ouvre un fichier dans un mode donné                  */
/*                                                                            */
/* En entrée : nomFic      chaine de caractères, nom du fichier à ouvrir      */
/*             mode        chaine de caractères, mode d'ouverture             */
/*                                                                            */
/* En sortie : le fichier ouvert                                              */
/*----------------------------------------------------------------------------*/

FILE* ouvertureFichier(char* nomFic, char* mode)
{
    FILE  * fic;

    fic = fopen(nomFic, mode);

    if (!fic)
    {
        printf("Problème lors de l'ouverture du fichier %s\n", nomFic);
    }
    return fic;
}

/*------------------------------------------------------------*/
/* afficherTab      affiche un tableau de caractères          */
/*                                                            */
/* En entrée :  charTab    tableau de caractères              */
/*              fichier    la sortie dans laquelle afficher   */
/*              taille     la taille du tableau               */
/*                                                            */
/* En sortie :  aucune                                        */
/*                                                            */
/* Localement : i          indice de charTab lors du parcours */
/*------------------------------------------------------------*/

void afficherTab(char* charTab, FILE* fichier, int taille)
{
    int   i;

    //Parcours du tableau caractère par caractère
    for (i=0; i<taille; i++)
    {
        fprintf(fichier, "%c", charTab[i]);
    }
}

/*--------------------------------------------------------------------------------------*/
/* compareTab                compare deux tableaux de caractères                        */
/*                                                                                      */
/* En entrée :  charTab1, charTab2   les deux tableaux à comparer                       */
/*              taille               la taille des deux tableaux                        */
/*                                                                                      */
/* En sortie :  res   -1 si charTab1 < charTab2                                         */
/*                     1 si charTab1 > charTab2                                         */
/*                     0 si charTab1 = charTab2                                         */
/*                                                                                      */
/* Localement : i      position dans les tableaux du caractère en cours de comparaison  */
/*--------------------------------------------------------------------------------------*/

int compareTab(char* charTab1, char* charTab2, int taille)
{
    int    i,
           res = 0;

    for (i=0; i<taille; i++)
    {

        if (charTab1[i] < charTab2[i])
        {
            res = -1;
            i = taille; //fin de la boucle
        }

        else if (charTab1[i] > charTab2[i])
        {
            res = 1;
            i = taille; //fin de la boucle
        }
    }

    return res;
}

/*-----------------------------------------------------------------------------------*/
/* contient         regarde si un tableau de caractères contient un motif            */
/*                                                                                   */
/* En entrée :  charTab   tableau de 10 caractères                                   */
/*              motif     chaîne de caractères                                       */
/*                                                                                   */
/* En sortie :  res       0 si le tableau ne contient pas le motif, 1 sinon          */
/*                                                                                   */
/* Localement : i         indice du caractère en cours de traitement dans le tableau */
/*              j         indice du caractère en cours de traitement dans le motif   */
/*-----------------------------------------------------------------------------------*/

int contient(char* charTab, char* motif)
{
    int   i = 0,    //indice du tableau
          j = 0,    //indice du motif
          res = 0;

    //Tant qu'on n'a pas parcouru tout le tableau (de taille 10)
    while (i<10)
    {
        //Tant que le tableau correspond au motif
        while (i<10 && charTab[i] == motif[j])
        {
            i++;
            j++;
        }

        //Si le motif est terminé (c'est à dire entièrement lu donc compris dans le tableau)
        if (motif[j] == '\0')
        {
            res = 1;
            i = 10;     //la boucle se termine
        }

        j=0;

        i++;
    }
    return res;
}